import supertest from 'supertest';
const request = supertest('https://gorest.co.in/public-api/');

import {
  expect
} from 'chai';

const TOKEN = '1377a842272313c9c0723c500fec152455b45a4592f5bf70bd2cd1817899b05b';

describe.skip('Users', () => {
  // it('GET /users', (done) => {
  //   request.get(`users?access-token=${TOKEN}`).end((err,res) => {
  //     expect(res.body.data).to.not.be.empty;
  //     done();
  //   });
  it('GET /users', () => {
    return request.get(`users?access-token=${TOKEN}`).then((res) => {
      expect(res.body.data).to.not.be.empty; //jshint ignore:line
    });
  });

  it('GET /users/:id', () => {
    return request.get(`users/1?access-token=${TOKEN}`).then((res) => {
      expect(res.body.data.id).to.be.eq(1);
    });
  });

  it('GET /users with query params', () => {
    const url = `users?access-token=${TOKEN}&page=5&gender=female&status=Active`;
    return request.get(url).then((res) => {
      expect(res.body.data).to.not.be.empty; //jshint ignore:line
      res.body.data.forEach(data => {
        expect(data.gender).to.eq('Female');
        expect(data.status).to.eq('Active');
      });
    });
  });

  it('POST /users ', () => {
    const data = {
      email: `test-${Math.floor(Math.random() * 9999)}@mail.ca`,
      name: 'Test name',
      gender: 'Male',
      status: 'Inactive',
    };

    return request
      .post('users')
      .set("Authorization", `Bearer ${TOKEN}`)
      .send(data)
      .then((res) => {
        expect(res.body.data).to.deep.include(data);
      });
  });

  it('PUT /users/:id', () => {
    const data = {
      name: `Luffy - ${Math.floor(Math.random() * 9999)}`,
      status: 'Active',
    };

    return request
      .put('users/1587')
      .set('Authorization', `Bearer ${TOKEN}`)
      .send(data)
      .then(res => {
        expect(res.body.data).to.deep.include(data);
      });
  });

  it('DELETE /users/:id', () => {
    return request
      .delete('users/2')
      .set('Authorization', `Bearer ${TOKEN}`)
      .then(res => {
        console.log(res.body);
        expect(res.body.data).to.be.eq(null);
      });

  });

});