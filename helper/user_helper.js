import supertest from 'supertest';
const request = supertest('https://gorest.co.in/public-api/');
const faker = require('faker');

import {
  expect
} from 'chai';

const TOKEN = '1377a842272313c9c0723c500fec152455b45a4592f5bf70bd2cd1817899b05b';

export const createRandomUserWithFaker = async () => { 
  const userData = {
      email: faker.internet.email(),
      name: faker.name.firstName(),
      gender: 'Male',
      status: 'Inactive',
    };
            
  const res = await request
  .post('users')
  .set("Authorization", `Bearer ${TOKEN}`)
  .send(userData)
  ;

  console.log(res.body);
  return res.body.data.id;
};  

export const createRandomUser = async () => { 
    const userData = {
        email: `test-${Math.floor(Math.random() * 9999)}@mail.ca`,
        name: 'Test name',
        gender: 'Male',
        status: 'Inactive',
      };
              
    const res = await request
    .post('users')
    .set("Authorization", `Bearer ${TOKEN}`)
    .send(userData)
    ;
    
    return res.body.data.id;
};